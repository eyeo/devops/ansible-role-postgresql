# PostgreSQL Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [PostgreSQL](https://www.postgresql.org/) resources.

## Synopsis

Below please find an example on how to include the role and configure the
individual PostgreSQL server instances in an Ansible Playbook:
	
    - hosts:
        "postgresql-servers"

      tasks:

        - import_role:
            name: "server"

      vars:

        postgresql_server_configuration:
          max_connections: "100"
          random_page_cost: "1.5"
          effective_cache_size: "256MB"
          log_min_duration_statement: "100"

        postgresql_service_strategy:
          "restart"

        postgresql_databases:
          example_1:
            encoding: "UTF8"
        
        postgresql_users:
          example_1:
            password: "changeme"

## Variables

All role variables are documented in the [`server/defaults/main.yml`](./server/defaults/main.yml) file.
